package com.diplo.tp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.util.regex.Pattern
import android.widget.Toast
import androidx.core.util.PatternsCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val boton: Button = findViewById(R.id.boton)
        val leyenda: TextView = findViewById(R.id.leyenda)
        val email: EditText = findViewById(R.id.email)
        val password: EditText = findViewById(R.id.password)

        var cancel = 0


        leyenda.visibility = View.INVISIBLE // Mensaje de entrada exitosa Invisible

        boton.setOnClickListener() {
            //CONDICIONES PARA VALIDACION DE CONTRASEÑA
            val RegexPsw = Pattern.compile(

                "^" +
                        "(?=.+[0-9])" +
                        "(?=.*[a-z])" +
                        "(?=.*[A-Z])" +
                        //LA SIGUIENTE LINEA ESTA COMENTADA PORQUE ME ROMPE LA VERIFICACION DE LA CONTRASEÑA
                        //"(?=.*[@#$%&+=!])" +
                        "(?=\\S+$)"+
                        ".{8,}" +
                        "$"
            )

            val mail: String = email.text.toString()
            val pasw: String = password.text.toString()

            email.error = null

            // EVITO CAMPOS VACIOS
            if ("" == mail) {
                email.error = "Ingresar Correo"
                leyenda.visibility = View.INVISIBLE
                email.requestFocus()
            } else if ("" == pasw) {
                password.error = "Ingresar Password"
                leyenda.visibility = View.INVISIBLE
                password.requestFocus()

                //VALIDACION DE CORREO
            } else if (!Patterns.EMAIL_ADDRESS.matcher(mail).matches()) {
                Toast.makeText(
                    getApplicationContext(),
                    "Correo Electronico Inválido, por favor verificar.",
                    Toast.LENGTH_SHORT
                ).show()
            }

            //VALIDACION DE CONTRASEÑA
            else if (!RegexPsw.matcher(pasw).matches()) {
                //CONFIGURO MENSAJE TIPO TOAST
                Toast.makeText(
                    getApplicationContext(),
                    "Contraseña Inválida, por favor verificar.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                leyenda.visibility = View.VISIBLE // Hago visible el mensaje de entrada exitosa
            }
        }
    }
}
